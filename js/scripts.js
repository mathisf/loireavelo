$('#connectButton').click(function() {
    $('#formConnect').addClass('active');
});
$('#connectButton2').click(function() {
    $('#formConnect').addClass('active');
});
$('#registerButton').click(function() {
    $('#formRegister').addClass('active');
});
$('.closeButton').click(function() {
    $('.modal').removeClass('active');
});
$('.hamburger').click(function() {
    $('.hamburger').toggleClass('is-active');
    toggleOverlay();
});
$('#tabphoto').click(function() {
    $('#tabfb').removeClass('active');
    $('#tabtrophee').removeClass('active');
    $('#tabvideo').removeClass('active');
    $('#tabphoto').addClass('active');
})
$('#tabvideo').click(function() {
    $('#tabfb').removeClass('active');
    $('#tabtrophee').removeClass('active');
    $('#tabvideo').addClass('active');
    $('#tabphoto').removeClass('active');
})
$('#tabtrophee').click(function() {
    $('#tabfb').removeClass('active');
    $('#tabtrophee').addClass('active');
    $('#tabvideo').removeClass('active');
    $('#tabphoto').removeClass('active');
})
$('#tabfb').click(function() {
    $('#tabfb').addClass('active');
    $('#tabtrophee').removeClass('active');
    $('#tabvideo').removeClass('active');
    $('#tabphoto').removeClass('active');
})

function toggleOverlay() {
    if ($('.overlay').hasClass('open')) {
        $('.overlay').removeClass('open');
        $('.overlay').addClass('close');
    } else {
        $('.overlay').addClass('open');
        $('.overlay').removeClass('close');
    }
}